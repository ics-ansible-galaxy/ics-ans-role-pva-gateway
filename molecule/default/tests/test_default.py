import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("all")


def test_pva_gateway_running_and_enabled(host):
    service = host.service("pva-gateway")
    assert service.is_enabled
    assert service.is_running


def test_pva_gateway_extra_args(host):
    processes = host.check_output("ps auwx")
    if (
        host.ansible.get_variables()["inventory_hostname"]
        == "ics-ans-role-pva-gateway-default"
    ):
        assert "pvagw /etc/pva-gateway/gateway.conf" in processes
    elif (
        host.ansible.get_variables()["inventory_hostname"]
        == "ics-ans-role-pva-gateway-args"
    ):
        assert "pvagw --no-ban-local /etc/pva-gateway/gateway.conf" in processes
