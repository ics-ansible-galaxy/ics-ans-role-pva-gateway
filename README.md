# ics-ans-role-pva-gateway

Ansible role to install a pvAccess gateway.

## Role Variables

```yaml
# pvagw is part of p4p
pva_gateway_version: 3.5.1
# User to run the pvAccess gateway
pva_gateway_user: pvagw

# Extra arguments to pass to the pvagw command
# pva_gateway_arguments: "--no-ban-local"
pva_gateway_arguments: ""

# pvAccess gateway configuration
# See https://mdavidsaver.github.io/p4p/gw.html#configuration-file
# for more information
pva_gateway_readonly: true
# List of clients
pva_gateway_clients:
  - name: theclient
    addrlist: "127.0.0.1 127.255.255.255"
    autoaddrlist: false
# List of servers
pva_gateway_servers:
  - name: theserver
    clients:
      - theclient
    interface: ["127.0.0.1"]
    addrlist: ""
    autoaddrlist: false
    statusprefix: "{{ ansible_hostname }}:GW:STS:"

# EPICS_PVA_MAX_ARRY_BYTES
pva_gateway_max_array: 10485760

# Required static routes
pva_gateway_static_routes: []
# Should be a list of dictionaries as below:
# pva_gateway_static_routes:
#   - dev: eth1
#     routes:
#       - 15.15.0.0/24 via 10.1.1.110
#       - 192.168.0.0/24 via 10.1.1.110
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-pva-gateway
```

## License

BSD 2-clause
